<?php

require __DIR__ . '/vendor/autoload.php';

use App\Plane;
use App\Car;
use App\Boat;
use App\Truck;

$vehicles = [
    new Car('bmw'),
    new Boat('boat'),
    new Plane('helicopter'),
    new Truck('kamaz')
];
foreach ($vehicles as $vehicle) {
    switch ($vehicle->name) {
        case 'bmw':
            $vehicle->move();
            $vehicle->musicOn();
            break;
        case 'boat':
            $vehicle->move();
            $vehicle->swim();
            break;
        case 'helicopter':
            $vehicle->takeOff();
            $vehicle->fly();
            $vehicle->landing();
            break;
        case 'kamaz':
            $vehicle->move();
            $vehicle->stop();
            $vehicle->emptyLoads();
            break;
    }

    $vehicle->stop();
    $vehicle->refuel('gas');
}
