<?php

namespace App;

use App\InterfaceHelper\VehicleAction;

class Bike extends Vehicle implements VehicleAction
{
    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function move()
    {
        echo $this->name . ' moving';
    }
}
