<?php

namespace App;

class Vehicle
{
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function refuel($fuel)
    {
        echo $this->name . ' refuel ' . $fuel;
    }

    public function stop()
    {
        echo $this->name . ' stopped';
    }
}
