<?php

namespace App;

use App\InterfaceHelper\CarAction;
use App\InterfaceHelper\VehicleAction;

class Car extends Vehicle implements CarAction, VehicleAction
{
    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function move()
    {
        echo $this->name . ' moving';
    }

    public function musicOn()
    {
        echo $this->name . ' music switched on';
    }
}
