<?php

namespace App\InterfaceHelper;

interface BoatAction
{
    public function swim();
}
