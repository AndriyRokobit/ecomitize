<?php

namespace App\InterfaceHelper;

interface VehicleAction
{
    public function move();
}
