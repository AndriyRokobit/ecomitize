<?php

namespace App\InterfaceHelper;

interface TruckAction
{
    public function emptyLoads();
}
