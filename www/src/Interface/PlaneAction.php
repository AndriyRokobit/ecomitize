<?php

namespace App\InterfaceHelper;

interface PlaneAction
{
    public function takeOff();

    public function landing();

    public function fly();
}
