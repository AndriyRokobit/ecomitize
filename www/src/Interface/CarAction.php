<?php

namespace App\InterfaceHelper;

interface CarAction
{
    public function musicOn();
}
