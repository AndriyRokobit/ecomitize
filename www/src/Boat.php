<?php

namespace App;

use App\InterfaceHelper\BoatAction;
use App\InterfaceHelper\VehicleAction;

class Boat extends Vehicle implements BoatAction, VehicleAction
{
    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function swim()
    {
        echo $this->name . ' swimming';
    }

    public function move()
    {
        echo $this->name . ' moving';
    }
}
