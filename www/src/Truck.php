<?php

namespace App;

use App\InterfaceHelper\TruckAction;
use App\InterfaceHelper\VehicleAction;

class Truck extends Vehicle implements TruckAction, VehicleAction
{
    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function move()
    {
        echo $this->name . ' moving';
    }

    /**
     * @param $fuel
     */
    public function emptyLoads()
    {
        echo $this->name . ' refuel' . "gasoline";
    }
}
