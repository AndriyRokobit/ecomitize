<?php

namespace App;

use App\InterfaceHelper\PlaneAction;

class Plane extends Vehicle implements PlaneAction
{
    public function __construct($name)
    {
        parent::__construct($name);
    }

    public function takeOff()
    {
        echo $this->name . ' took off';
    }

    public function landing()
    {
        echo $this->name . ' landing';
    }

    public function fly()
    {
        echo $this->name . ' flying';
    }
}